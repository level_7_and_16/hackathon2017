import sqlite3

con = sqlite3.connect("data_text.db")
sql = u"""
create table text_emo (
  time              TEXT,
  sec               TEXT,
  content           TEXT,
  score             REAL
);
"""
con.execute(sql)
con.close()
