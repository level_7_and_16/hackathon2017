# Imports the Google Cloud client library
import sys
from datetime import datetime
import sqlite3
from google.cloud import language
from google.cloud.language import enums
from google.cloud.language import types

# Instantiates a client
client = language.LanguageServiceClient()

# The text to analyze
# text = '楽しい'
text = sys.argv[1]
document = types.Document(
    content=text,
    type=enums.Document.Type.PLAIN_TEXT)

# Detects the sentiment of the text
sentiment = client.analyze_sentiment(document=document)

# print('Text: {}'.format(text))
# print('Sentiment: {}, {}'.format(sentiment.score, sentiment.magnitude))
# print(sentiment)

print("total score:{:.2f}".format(sentiment.document_sentiment.score))

for cont in sentiment.sentences:
    nowtime=datetime.now().strftime("%Y%m%d_%H%M%S")
    content=cont.text.content
    score=cont.sentiment.score

    print(nowtime)
    print("{}:score:{:.2f}".format(cont.text.content,cont.sentiment.score))

    con = sqlite3.connect("data_text.db")
    c = con.cursor()
    sql = ('insert into text_emo(time,content,score) values (?,?,?)')
    user = (nowtime,content,score)
    c.execute(sql, user)
    con.commit()
    con.close()

