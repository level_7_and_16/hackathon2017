import sys
import json
import requests
import json
import cv2
import time
from datetime import datetime
from azure.storage.blob import BlockBlobService
from azure.storage.blob import ContentSettings
import sqlite3


url = 'https://api.projectoxford.ai/face/v1.0/detect'
base_url = 'https://cs47b0ec473edb3x4956xbe9.blob.core.windows.net/mycontainer/'

headers = {
    'Content-Type': 'application/json',
    'Ocp-Apim-Subscription-Key': '10ac77671b484076b430959e707d5e4d'
}
params = {
    'returnFaceId': 'true',  # The default value is true.
    'returnFaceLandmarks': 'false', # The default value is false.
    'returnFaceAttributes': 'age,gender,headPose,smile,facialHair,glasses,emotion,hair,makeup,occlusion,accessories,blur,exposure,noise',
}


# azure strageに接続するオブジェクトを作成
block_blob_service = BlockBlobService(account_name='cs47b0ec473edb3x4956xbe9', account_key='dyahQc2E8dOP6rR3JqhpAhCj0qUr/zputYscmuBd3qVj8vWjwnatSUKV2NKyiuw1MGdMeSqe7sWByuLYTrYgCQ==')

# PCのカメラをキャプチャするオブジェクト作成
capture = cv2.VideoCapture(0)

if capture.isOpened() is False:
    raise("IO Error")

    
def get_pic_and_upload():
    ret, image = capture.read()

    # if ret == False:
    #     continue

    # ファイル出力
    fname = datetime.now().strftime("%Y%m%d_%H%M%S.png")
    cv2.imwrite(fname, image)

    #  保存したファイルを azure storage の mycontainerに保存する
    block_blob_service.create_blob_from_path(
        'mycontainer',
        fname,
        fname,
        content_settings=ContentSettings(content_type='image/png')
        )

    return fname


while True:
    #  画像を撮影して、アップロードする
    fname = get_pic_and_upload()

    #  1秒スリープ
    # time.sleep(1)

    payload = {
        'url': base_url + fname,
    }
    r = requests.post(url ,headers = headers, params = params, data = json.dumps(payload))

    json_dict = json.loads(r.text)

    try:
        # ファイル名から".png"を削除して時間にしている
        time = fname[0:-4]
        gender  = json_dict[0]['faceAttributes']['gender']
        emotion_sadness   = json_dict[0]['faceAttributes']['emotion']['sadness']
        emotion_happiness = json_dict[0]['faceAttributes']['emotion']['happiness']
        emotion_surprise  = json_dict[0]['faceAttributes']['emotion']['surprise']
        emotion_fear      = json_dict[0]['faceAttributes']['emotion']['fear']
        emotion_neutral   = json_dict[0]['faceAttributes']['emotion']['neutral']
        emotion_anger     = json_dict[0]['faceAttributes']['emotion']['anger']
        emotion_disgust   = json_dict[0]['faceAttributes']['emotion']['disgust']
        age  = json_dict[0]['faceAttributes']['age']    

        print("time: "+ time)
        print("gender: " + gender)
        print("emotion_sadness: " + str(emotion_sadness))
        print("emotion_happiness: " + str(emotion_happiness))
        print("emotion_surprise: " + str(emotion_surprise))
        print("emotion_fear: " + str(emotion_fear))
        print("emotion_neutral: " + str(emotion_neutral))
        print("emotion_anger: " + str(emotion_anger))
        print("emotion_disgust: " + str(emotion_disgust))
        print("age: " + str(age))

        
        # SQL文に値をセットする場合は，Pythonのformatメソッドなどは使わずに，
        # セットしたい場所に?を記述し，executeメソッドの第2引数に?に当てはめる値を
        # タプルで渡す．
        con = sqlite3.connect("data.db")
        c = con.cursor()
        sql = ('insert into face (time, '
                                'gender           ,'
                                'emotion_sadness   ,'
                                'emotion_happiness ,'
                                'emotion_surprise  ,'
                                'emotion_fear      ,'
                                'emotion_neutral   ,'
                                'emotion_anger     ,'
                                'emotion_disgust   ,'
                                'age'
                                ') values (?,?,?,?,?,?,?,?,?,?)')
        user = (time,
                gender,
                emotion_sadness   ,
                emotion_happiness ,
                emotion_surprise  ,
                emotion_fear      ,
                emotion_neutral   ,
                emotion_anger     ,
                emotion_disgust   ,
                age)
        c.execute(sql, user)
        con.commit()
        con.close()

    except Exception as e:
        # print("[Errno {0}] {1}".format(e.errno, e.strerror))
        continue

# ↓取得できるデータの例
# [{'faceRectangle': {'width': 201, 'top': 105, 'height': 201, 'left': 237}, 'faceId': '98e1531d-1481-4350-aaf6-933834615573', 'faceAttributes': {'gender': 'male', 'facialHair': {'sideburns': 0.1, 'bear
# d': 0.0, 'moustache': 0.0}, 'emotion': {'sadness': 0.046, 'contempt': 0.006, 'happiness': 0.002, 'surprise': 0.004, 'fear': 0.0, 'neutral': 0.938, 'anger': 0.0, 'disgust': 0.003}, 'blur': {'value': 0.
# 43, 'blurLevel': 'medium'}, 'noise': {'value': 0.43, 'noiseLevel': 'medium'}, 'hair': {'hairColor': [{'color': 'black', 'confidence': 0.98}, {'color': 'brown', 'confidence': 0.96}, {'color': 'gray', '
# confidence': 0.32}, {'color': 'red', 'confidence': 0.17}, {'color': 'other', 'confidence': 0.13}, {'color': 'blond', 'confidence': 0.06}], 'bald': 0.09, 'invisible': False}, 'accessories': [{'confiden
# ce': 1.0, 'type': 'glasses'}], 'glasses': 'ReadingGlasses', 'occlusion': {'mouthOccluded': False, 'foreheadOccluded': False, 'eyeOccluded': False}, 'makeup': {'eyeMakeup': False, 'lipMakeup': False},
# 'headPose': {'yaw': -7.2, 'pitch': 0.0, 'roll': 4.8}, 'smile': 0.002, 'exposure': {'exposureLevel': 'overExposure', 'value': 0.76}, 'age': 32.2}}]

