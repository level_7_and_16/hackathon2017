# みずほ銀行 APIのサンプル
# 口座照会
import sys
import requests
import json


url = 'https://api.us.apiconnect.ibmcloud.com/api-banking-for-mizuho-dev03/finolab/retail/accounts/v2'
headers = {
    # Authorization は顧客識別子
    'Authorization': '240', 
    'X-IBM-Client-Id': '56f3a523-2eff-45bd-8833-124877b4b810',
}

try:
    r = requests.get(url ,headers = headers)
    print(r.text)

except Exception as e:
    print("[Errno {0}] {1}".format(e.errno, e.strerror))


# ↓取得できるデータの例
# {"accountsList":[{"accountId":"241","bankCode":"0001","bankName":"みずほ銀行","branchNumber":"132","branchKanjiName":"中目黒支店","branchName":"ﾅｶﾒｸﾞﾛｼﾃﾝ","accountType":"1","additionalAccountType":"普
# 通預金","currencyCode":"JPY","accountNumber":"2412400","accountName":"ﾅｶﾒｸﾞﾛﾀﾛｳ"},{"accountId":"242","bankCode":"0001","bankName":"みずほ銀行","branchNumber":"132","branchKanjiName":"中目黒支店","bran
# chName":"ﾅｶﾒｸﾞﾛｼﾃﾝ","accountType":"9","additionalAccountType":"カードローン","currencyCode":"JPY","accountNumber":"2422400","accountName":"ﾅｶﾒｸﾞﾛﾀﾛｳ"}],"customerInformation":""}

