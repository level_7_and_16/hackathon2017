# PCのカメラから1秒おきにスナップショットをとって保存する

import cv2
import time
from datetime import datetime

if __name__=="__main__":

    capture = cv2.VideoCapture(0)
    
    if capture.isOpened() is False:
        raise("IO Error")

    while True:
        ret, image = capture.read()

        if ret == False:
            continue

        # ファイル出力
        fname = datetime.now().strftime("%Y%m%d_%H%M%S.png")
        cv2.imwrite(fname, image)
        
        #  1秒スリープ
        time.sleep(1)

