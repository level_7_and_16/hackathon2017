# PCのカメラから1秒おきにスナップショットをとってazure storageに保存するツール

import cv2
import time
from datetime import datetime
from azure.storage.blob import BlockBlobService
from azure.storage.blob import ContentSettings

if __name__=="__main__":
    # azure strageに接続するオブジェクトを作成
    block_blob_service = BlockBlobService(account_name='cs47b0ec473edb3x4956xbe9', account_key='dyahQc2E8dOP6rR3JqhpAhCj0qUr/zputYscmuBd3qVj8vWjwnatSUKV2NKyiuw1MGdMeSqe7sWByuLYTrYgCQ==')

    # PCのカメラをキャプチャするオブジェクト作成
    capture = cv2.VideoCapture(0)
    
    if capture.isOpened() is False:
        raise("IO Error")

    while True:
        ret, image = capture.read()

        if ret == False:
            continue

        # ファイル出力
        fname = datetime.now().strftime("%Y%m%d_%H%M%S.png")
        cv2.imwrite(fname, image)

        #  保存したファイルを azure storage の mycontainerに保存する
        block_blob_service.create_blob_from_path(
            'mycontainer',
            fname,
            fname,
            content_settings=ContentSettings(content_type='image/png')
            )

        #  1秒スリープ
        time.sleep(1)



