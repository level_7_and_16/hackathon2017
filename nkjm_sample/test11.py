# matplotlibでリアルタイム描画するサンプル
from __future__ import unicode_literals, print_function
import sys
import json
import requests
import json
import cv2
import time
from datetime import datetime
from azure.storage.blob import BlockBlobService
from azure.storage.blob import ContentSettings



import numpy as np
import matplotlib.pyplot as plt




url = 'https://api.projectoxford.ai/face/v1.0/detect'
base_url = 'https://cs47b0ec473edb3x4956xbe9.blob.core.windows.net/mycontainer/'

headers = {
    'Content-Type': 'application/json',
    'Ocp-Apim-Subscription-Key': '10ac77671b484076b430959e707d5e4d'
}
params = {
    'returnFaceId': 'true',  # The default value is true.
    'returnFaceLandmarks': 'false', # The default value is false.
    'returnFaceAttributes': 'age,gender,headPose,smile,facialHair,glasses,emotion,hair,makeup,occlusion,accessories,blur,exposure,noise',
}


block_blob_service = BlockBlobService(account_name='cs47b0ec473edb3x4956xbe9', account_key='dyahQc2E8dOP6rR3JqhpAhCj0qUr/zputYscmuBd3qVj8vWjwnatSUKV2NKyiuw1MGdMeSqe7sWByuLYTrYgCQ==')

# PCのカメラをキャプチャするオブジェクト作成
capture = cv2.VideoCapture(0)
if capture.isOpened() is False:
    raise("IO Error")

fig, ax = plt.subplots(1, 1)
x = np.arange(-np.pi, np.pi, 1)
y = np.sin(x)

# 初期化的に一度plotしなければならない
# そのときplotしたオブジェクトを受け取る受け取る必要がある．
# listが返ってくるので，注意
lines, = ax.plot(x, y)

def get_pic_and_upload():
    ret, image = capture.read()

    # if ret == False:
    #     continue

    # ファイル出力
    fname = datetime.now().strftime("%Y%m%d_%H%M%S.png")
    cv2.imwrite(fname, image)

    #  保存したファイルを azure storage の mycontainerに保存する
    block_blob_service.create_blob_from_path(
        'mycontainer',
        fname,
        fname,
        content_settings=ContentSettings(content_type='image/png')
        )

    return fname


while True:
    #  画像を撮影して、アップロードする
    fname = get_pic_and_upload()

    #  1秒スリープ
    # time.sleep(1)

    payload = {
        'url': base_url + fname,
    }
    r = requests.post(url ,headers = headers, params = params, data = json.dumps(payload))

    try:
        json_dict = json.loads(r.text)

        # happinessの値を取得する例
        happiness = json_dict[0]['faceAttributes']['emotion']['happiness'] * 100
        print(x)
        print(happiness)

        x += 1
        y = np.sin(x)

        # 描画データを更新するときにplot関数を使うと
        # lineオブジェクトが都度増えてしまうので，注意．
        #
        # 一番楽なのは上記で受け取ったlinesに対して
        # set_data()メソッドで描画データを更新する方法．
        lines.set_data(x, y)

        # set_data()を使うと軸とかは自動設定されないっぽいので，
        # 今回の例だとあっという間にsinカーブが描画範囲からいなくなる．
        # そのためx軸の範囲は適宜修正してやる必要がある．
        ax.set_xlim((x.min(), x.max()))

        # 一番のポイント
        # - plt.show() ブロッキングされてリアルタイムに描写できない
        # - plt.ion() + plt.draw() グラフウインドウが固まってプログラムが止まるから使えない
        # ----> plt.pause(interval) これを使う!!! 引数はsleep時間
        plt.pause(1)


    except Exception as e:
        continue

