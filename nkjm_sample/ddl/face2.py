import sqlite3

#  後で特定の個人を判断する区分を追加するかも

con = sqlite3.connect("data2.db")
sql = u"""
create table face (
  time              TEXT,
  sec               INTEGER,
  emotion_sadness   REAL,
  emotion_happiness REAL,
  emotion_surprise  REAL,
  emotion_fear      REAL,
  emotion_neutral   REAL,
  emotion_anger     REAL,
  emotion_disgust   REAL
);
"""
con.execute(sql)
con.close()
