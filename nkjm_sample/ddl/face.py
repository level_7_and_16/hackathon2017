import sqlite3

#  後で特定の個人を判断する区分を追加するかも

con = sqlite3.connect("data.db")
sql = u"""
create table face (
  time              TEXT,
  gender            TEXT,
  emotion_sadness   REAL,
  emotion_happiness REAL,
  emotion_surprise  REAL,
  emotion_fear      REAL,
  emotion_neutral   REAL,
  emotion_anger     REAL,
  emotion_disgust   REAL,
  age               REAL
);
"""
con.execute(sql)
con.close()
