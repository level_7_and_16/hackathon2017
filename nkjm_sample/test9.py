import sys
import json
import requests


url = 'https://api.projectoxford.ai/face/v1.0/detect'
# image_url = 'https://upload.wikimedia.org/wikipedia/commons/c/c3/RH_Louise_Lillian_Gish.jpg'
image_url = 'https://cs47b0ec473edb3x4956xbe9.blob.core.windows.net/mycontainer/20170729_140651.png'

headers = {
    'Content-Type': 'application/json',
    'Ocp-Apim-Subscription-Key': '10ac77671b484076b430959e707d5e4d'
}
params = {
    'returnFaceId': 'true',  # The default value is true.
    'returnFaceLandmarks': 'false', # The default value is false.
    'returnFaceAttributes': 'age,gender,headPose,smile,facialHair,glasses,emotion,hair,makeup,occlusion,accessories,blur,exposure,noise',
}

payload = {
    'url': image_url,
}

if __name__ == '__main__':
    r = requests.post(url ,headers = headers, params = params, data = json.dumps(payload))

    print(r.text)